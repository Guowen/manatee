/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CORE_NEW_ORDER_SINGLE_H_
#define SRC_CORE_NEW_ORDER_SINGLE_H_

#include <string>
#include "enumeration.h"

namespace manatee {

struct NewOrderSingle {
 public:
  std::string cl_ord_id;
  std::string account_id;
  OrdType ord_type;
  Side side;
  std::string security_id;
  std::string security_id_source;
  double order_qty;
  double price;
};

}

#endif /* SRC_CORE_NEW_ORDER_SINGLE_H_ */
