/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CORE_SECURITY_H_
#define SRC_CORE_SECURITY_H_

#include <string>

namespace manatee {

class Security : public Model {
 public:
  Security() :
    type_(0), sub_type_(0), exchange_type_(0), status_(0) {}
  virtual ~Security() {}

  const std::string& symbol() const { return symbol_; }
  const int type() const { return type_; }
  const int sub_type() const { return sub_type_; }
  const int exchange_type() const { return exchange_type_; }
  const int status() const { return status_; }

 private:
  std::string symbol_;
  int type_;
  int sub_type_;
  int exchange_type_;
  int status_;
};

}  // namespace manatee

#endif /* SRC_CORE_SECURITY_H_ */
