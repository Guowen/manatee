/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CORE_ORDER_H_
#define SRC_CORE_ORDER_H_

#include <string>
#include <functional>

#include "enumeration.h"
#include "model.h"

namespace manatee {

struct NewOrderSingle;
struct OrderCancelRequest;
struct ExecutionReport;
struct OrderCancelReject;

class FareResult;
class RiskResult;

class Account;

class Order;
typedef std::function<void (Order*, int)> StateChangedCallback;

class Order : public Model {
 public:
  enum State {
    STATE_CREATED,
    STATE_CALCULATED,
    STATE_RISKED,
    STATE_OPENED,
    STATE_DONE,
    STATE_CANCELLED,

    NULL_VAL = 255,
  };

  Order();
  virtual ~Order();

  void set_order_id(const std::string& order_id) {
    order_id_ = order_id;
  }

  const std::string& order_id() const {
    return order_id_;
  }

  const std::string& account_id() const {
    return account_id_;
  }

  const State state() const {
    return state_;
  }

  const State prev_state() const {
    return prev_state_;
  }

  void SetStateChangedCallback(const StateChangedCallback& cb) {
    cb_ = cb;
  }

  void OnNewOrderSingle(NewOrderSingle* request);
  void OnOrderCancelRequest(OrderCancelRequest* request);
  void OnExecutionReport(ExecutionReport* report);
  void OnOrderCancelReject(OrderCancelReject* reject);

  void OnCalcResult(FareResult* result);
  void OnRiskResult(RiskResult* result);

 private:
  void OnOpened();
  void OnDone();

  StateChangedCallback cb_;

  std::string account_id_;
  std::string order_id_;
  std::string cli_ord_id_;

  Account* security_account_;
  Account* capital_account_;

  State state_;
  State prev_state_;

  OrdType ord_type_;
  OrdStatus ord_status_;
  Side side_;

  double price_;
  TimeInForce tif_;
  long transact_time_;

  double avg_px_;
  double cum_qty_;
  double last_px_;
  double last_qty_;
};

} // namespace manatee

#endif /* SRC_CORE_ORDER_H_ */
