/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "order.h"

#include "new_order_single.h"
#include "order_cancel_request.h"
#include "execution_report.h"
#include "order_cancel_reject.h"

namespace manatee {

Order::Order() :
  security_account_(nullptr),
  capital_account_(nullptr),
  state_(NULL_VAL),
  prev_state_(NULL_VAL),
  ord_type_(OrdType::NULL_VAL),
  ord_status_(OrdStatus::NULL_VAL),
  side_(Side::NULL_VAL),
  price_(0.0),
  tif_(TimeInForce::NULL_VAL),
  transact_time_(0),
  avg_px_(0.0),
  cum_qty_(0.0),
  last_px_(0.0),
  last_qty_(0.0) {
}

Order::~Order() {
  // TODO Auto-generated destructor stub
}

void Order::OnNewOrderSingle(NewOrderSingle* request) {
  account_id_ = request->account_id;
  ord_type_ = request->ord_type;
  side_ = request->side;
  price_ = request->price;

  state_ = STATE_CREATED;
  cb_(this, STATE_CREATED);
}

void Order::OnOrderCancelRequest(OrderCancelRequest* request) {}

void Order::OnExecutionReport(ExecutionReport* report) {
  prev_state_ = state_;
  state_ = STATE_OPENED;
  cb_(this, STATE_OPENED);
  return;

  if (report->exec_type == ExecType::NEW) {
    // opened in remote
    state_ = STATE_OPENED;
    cb_(this, STATE_OPENED);

    // is completely filled
    state_ = STATE_DONE;
    cb_(this, STATE_DONE);
  } else {
    // cancelled in remote

    state_ = STATE_CANCELLED;
    cb_(this, STATE_CANCELLED);
  }
}

void Order::OnDone() {

}

void Order::OnOrderCancelReject(OrderCancelReject* reject) {

}

void Order::OnCalcResult(FareResult* result) {
  prev_state_ = state_;
  state_ = STATE_CALCULATED;
  cb_(this, STATE_CALCULATED);
}

void Order::OnRiskResult(RiskResult* result) {
  prev_state_ = state_;
  state_ = STATE_RISKED;
  cb_(this, STATE_RISKED);
}

}
