/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CORE_ENUMERATION_H_
#define SRC_CORE_ENUMERATION_H_

namespace manatee {

class OrdStatus {
 public:
  enum Status {
    NEW = '0',
    PARTIALLY_FILLED = '1',
    FILLED = '2',
    DONE_FOR_DAY = '3',
    CANCELED = '4',
    REPLACED = '5',
    PENDING_CANCEL = '6',
    STOPPED = '7',
    REJECTED = '8',
    SUSPENDED = '9',
    PENDING_NEW = 'A',
    CALCULATED = 'B',
    EXPIRED = 'C',
    ACCEPTED_FOR_BIDDING = 'D',
    PENDING_REPLACE = 'E',
    RISKED = 'W',
    PARTIALLY_CANCELED = 'V',
    INIT = 'Y',
    CREATE = 'X',

    NULL_VAL = 255
  };

  OrdStatus()
      : status_(NULL_VAL) {
  }

  explicit OrdStatus(const OrdStatus& status) {
    status_ = status.status();
  }

  explicit OrdStatus(Status status) {
    status_ = status;
  }

  void operator=(const OrdStatus& status) {
    status_ = status.status();
  }

  bool operator==(const OrdStatus& status) const {
    return status_ == status.status();
  }

  Status status() const {
    return status_;
  }

 private:
  Status status_;
};

class Side {
 public:
  enum TypeSide {
    BUY,
    SELL,

    NULL_VAL = 255,
  };

  Side()
      : side_(NULL_VAL) {
  }

  explicit Side(TypeSide side) {
    side_ = side;
  }

  explicit Side(const Side& side) {
    side_ = side.side_;
  }

  void operator=(const Side& rhs) {
    side_ = rhs.side();
  }

  bool operator==(const Side& rhs) const {
    return side_ == rhs.side();
  }

  TypeSide side() const {
    return side_;
  }

 private:
  TypeSide side_;
};

class TimeInForce {
 public:
  enum TIF {
    DAY = '0',
    GOOD_TILL_CANCEL = '1',
    AT_THE_OPENING = '2',
    IMMEDIATE_OR_CANCEL = '3',
    FILL_OR_KILL = '4',
    GOOD_TILL_CROSSING = '5',
    GOOD_TILL_DATE = '6',
    AT_THE_CLOSE = '7',
    GOOD_THROUGH_CROSSING = '8',
    AT_CROSSING = '9',

    NULL_VAL = 255,
  };

  TimeInForce()
      : tif_(NULL_VAL) {
  }

  explicit TimeInForce(TIF tif) {
    tif_ = tif;
  }

 private:
  TIF tif_;
};

class OrdType {
 public:
  enum Type {
    MARKET = '1',
    LIMIT = '2',
    STOP = '3',
    STOP_LIMIT = '4',
    MARKET_ON_CLOSE = '5',
    WITH_OR_WITHOUT = '6',
    LIMIT_OR_BETTER = '7',
    LIMIT_WITH_OR_WITHOUT = '8',
    ON_BASIS = '9',
    ON_CLOSE = 'A',
    LIMIT_ON_CLOSE = 'B',
    FOREX_MARKET = 'C',
    PREVIOUSLY_QUOTED = 'D',
    PREVIOUSLY_INDICATED = 'E',
    FOREX_LIMIT = 'F',
    FOREX_SWAP = 'G',
    FOREX_PREVIOUSLY_QUOTED = 'H',
    FUNARI = 'I',
    MARKET_IF_TOUCHED = 'J',
    MARKET_WITH_LEFT_OVER_AS_LIMIT = 'K',
    PREVIOUS_FUND_VALUATION_POINT = 'L',
    NEXT_FUND_VALUATION_POINT = 'M',
    PEGGED = 'P',
    COUNTER_ORDER_SELECTION = 'Q',

    NULL_VAL = 255,
  };

  OrdType()
      : type_(NULL_VAL) {
  }

  explicit OrdType(Type type)
      : type_(type) {
  }

  explicit OrdType(const OrdType& type) {
    type_ = type.type_;
  }

  void operator=(Type type) {
    type_ = type;
  }

  bool operator==(const OrdType& rhs) const {
    return type_ == rhs.type_;
  }

 private:
  Type type_;
};

class ExecType {
 public:
  enum Type {
    NEW,

    NULL_VAL = 255,
  };

  ExecType()
      : type_(NULL_VAL) {
  }

  bool operator==(Type type) const {
    return type_ == type;
  }

  Type type() const {
    return type_;
  }

 private:
  Type type_;
};

}

#endif /* SRC_CORE_ENUMERATION_H_ */
