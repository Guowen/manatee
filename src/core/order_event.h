/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CORE_ORDER_EVENT_H_
#define SRC_CORE_ORDER_EVENT_H_

namespace manatee {

class OrderEvent {
 public:
  enum Event {
    STATE_UPDATED,
  };

  explicit OrderEvent(Event event)
      : event_(event) {
  }

  Event event() const {
    return event_;
  }

 private:
  Event event_;
};

}

#endif /* SRC_CORE_ORDER_EVENT_H_ */
