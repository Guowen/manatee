/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CORE_AGGREGATE_H_
#define SRC_CORE_AGGREGATE_H_

#include <map>

namespace manatee {

template <typename T>
class Aggregate {
 public:
  Aggregate() {}
  virtual ~Aggregate() {}

  T* find(long entity_id) {
    auto it = entities_.find(entity_id);
    return it == entities_.end() ? nullptr : it->second;
  }

  bool Insert(T* entity) {
    long entity_id = entity->entity_id();
    return entities_.insert(std::make_pair(entity_id, entity));
  }

  bool Erase(long entity_id) {
    auto it = entities_.erase(entity_id);
    if (it == entities_.end()) {
      return false;
    }

    delete it->second;
    return true;
  }

 private:
  std::map<long, T*> entities_;
};

}

#endif /* SRC_CORE_AGGREGATE_H_ */
