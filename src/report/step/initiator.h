/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_REPORT_STEP_INITIATOR_H_
#define SRC_REPORT_STEP_INITIATOR_H_

#include <glog/logging.h>

namespace manatee {
namespace report {

class Session;

class InitiatorListener {
 public:
  virtual ~InitiatorListener() {}
  virtual void OnInitiatorReady() = 0;
  virtual void OnSessionCreated(Session* session) = 0;
};

class Initiator {
 public:
  virtual ~Initiator() {}

  void set_config_file();
  void set_listener(InitiatorListener* listener) {
    CHECK(listener != nullptr);
    CHECK(listener_ == nullptr);
  }

  virtual bool Init();
  virtual void Start();
  virtual void Stop();

 protected:
  InitiatorListener* listener_;
};

}
}



#endif /* SRC_REPORT_STEP_INITIATOR_H_ */
