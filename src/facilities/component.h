/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_FACILITIES_COMPONENT_H_
#define SRC_FACILITIES_COMPONENT_H_

#include "core/aggregate.h"

namespace manatee {

template <typename E, typename T>
class ComponentListener {
 public:
  ComponentListener() {}
  virtual ~ComponentListener() {}
  virtual void OnComponentEvent(E event, T model) = 0;
};

template <typename E, typename T>
class Component {
 public:
  void set_aggregate(Aggregate<T>* aggregate) {
    aggregate_ = aggregate;
  }

  Aggregate<T> getAggregate() {
    return aggregate_;
  }

  void BeginWatch(ComponentListener<E, T>* listener) {
    for (int i = 0; i < listeners_.size(); ++i) {
      if (listeners_[i] == listener) {
        return;
      }
    }

    listeners_.push_back(listener);
  }

  void EndWatch(ComponentListener<E, T>* listener) {}

  void FireEvent(E e, T t) {
    for (int i = 0; i < listeners_.size(); ++i) {
      listeners_[i]->OnComponentEvent(e, t);
    }
  }

 private:
  std::unique_ptr<Aggregate<T>*> aggregate_;
  std::vector<ComponentListener<E, T>*> listeners_;
};

}

#endif /* SRC_FACILITIES_COMPONENT_H_ */
