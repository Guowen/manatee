/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_ORDER_RISK_PROCESSOR_H_
#define SRC_ORDER_RISK_PROCESSOR_H_

namespace manatee {

class RiskProcessor {
 public:
  RiskProcessor();
  virtual ~RiskProcessor();

  void set_order_component(
      const std::shared_ptr<OrderComponent>& order_component);

  virtual void OnInit();
  virtual void OnProcess(octopus::Message* message);
};

}  // namespace manatee

#endif /* SRC_ORDER_RISK_PROCESSOR_H_ */
