/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_ORDER_ORDER_PROCESSOR_H_
#define SRC_ORDER_ORDER_PROCESSOR_H_

#include <string>

#include "octopus/message.h"
#include "octopus/base_ring_processor.h"

namespace manatee {

struct NewOrderSingle;
struct OrderCancelRequest;
struct ExecutionReport;
struct OrderCancelReject;

class FareResult;
class RiskResult;

class OrderAggregate;
class OrderComponent;
class Account;

class OrderProcessor : public octopus::BaseRingProcessor {
 public:
  OrderProcessor();
  virtual ~OrderProcessor();

  void set_order_component(
      const std::shared_ptr<OrderComponent>& order_component);

  virtual void Process(octopus::Message* message);

 private:
  void OnNewOrderSingle(NewOrderSingle* request);
  void OnOrderCancelRequest(OrderCancelRequest* request);
  void OnExecutionReport(ExecutionReport* report);
  void OnOrderCancelReject(OrderCancelReject* reject);

  void OnCalcResult(FareResult* result);
  void OnRiskResult(RiskResult* result);

  OrderAggregate* aggregate();

  std::shared_ptr<OrderComponent> order_component_;
  std::map<std::string, Account*> accounts_;

};

}  // namespace manatee

#endif /* SRC_ORDER_ORDER_PROCESSOR_H_ */
