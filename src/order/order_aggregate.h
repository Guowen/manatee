/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_ORDER_ORDER_AGGREGATE_H_
#define SRC_ORDER_ORDER_AGGREGATE_H_

#include <map>

namespace manatee {

class Order;

class OrderAggregate {
 public:
  OrderAggregate();
  virtual ~OrderAggregate();

  Order* CreateModel();
  bool FillModel(Order* order);
  void PutModel(Order* order);
  bool RemoveModel(Order* order);
  bool RemoveModel(long id);
  Order* GetModel(long id);
  void RecycleModel(Order* order);

 private:
  long next_id_;
  std::map<long, Order*> orders_;
};

}  // namespace manatee

#endif /* SRC_ORDER_ORDER_AGGREGATE_H_ */
