/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_ORDER_ORDER_EVENT_PROCESSOR_H_
#define SRC_ORDER_ORDER_EVENT_PROCESSOR_H_

#include <glog/logging.h>
#include "octopus/message.h"
#include "octopus/processor.h"
#include "core/order_event.h"
#include "order_component.h"

namespace manatee {

class Order;

class OrderEventProcessor : public octopus::Processor {
 public:
  OrderEventProcessor();
  virtual ~OrderEventProcessor();

  void set_order_component(const std::shared_ptr<OrderComponent>& component) {
    CHECK(component != nullptr);
    order_component_ = component;
  }

  virtual void OnInit();
  virtual void Process(octopus::Message* message);
  virtual void OnIdle(long count);

  void OnOrderEvent(OrderEvent event, Order* order);
  void OnCreated(Order* order);
  void OnCalculated(Order* order);
  void OnRisked(Order* order);
  void OnOpened(Order* order);
  void OnDone(Order* order);
  void OnCancelled(Order* order);

 private:
  std::shared_ptr<OrderComponent> order_component_;
  std::vector<long> completed_;

};

}  // namespace manatee

#endif /* SRC_ORDER_ORDER_EVENT_PROCESSOR_H_ */
