/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <functional>
#include <glog/logging.h>
#include "order_component.h"
#include "order_aggregate.h"

#include "core/new_order_single.h"
#include "core/execution_report.h"

namespace manatee {

OrderComponent::OrderComponent()
    : cb_(std::bind(&OrderComponent::OnStateChanged, this,
                    std::placeholders::_1, std::placeholders::_2)),
      aggregate_(new OrderAggregate()) {
}

OrderComponent::~OrderComponent() {
}

void OrderComponent::OnNewOrderSingle(NewOrderSingle* request, Order* order) {
  order->SetStateChangedCallback(cb_);

  order->OnNewOrderSingle(request);
}

void OrderComponent::OnExecutionReport(ExecutionReport* report) {
  long order_id = ParseOrderID(report->cl_ord_id);
  Order* order = aggregate_->GetModel(order_id);
  CHECK(order != nullptr);

  order->OnExecutionReport(report);
}

void OrderComponent::OnStateChanged(Order* order, int state) {
  VLOG(100) << "OnStateChanged: " << order->order_id() << ", "
            << order->prev_state() << " --> " << order->state();

  OrderEvent event(OrderEvent::STATE_UPDATED);
  FireEvent(event, order);
}

long OrderComponent::ParseOrderID(const std::string& cl_ord_id) {
  return atol(cl_ord_id.c_str());
}

}
