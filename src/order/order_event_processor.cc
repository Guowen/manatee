/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "order_event_processor.h"

#include <glog/logging.h>
#include "core/order.h"
#include "core/fare_result.h"
#include "core/risk_result.h"
#include "core/execution_report.h"

#include "facilities/component.h"
#include "order/order_aggregate.h"

namespace manatee {

class OrderEventListener : public ComponentListener<OrderEvent, Order*> {
 public:
  OrderEventListener(OrderEventProcessor* processor)
      : processor_(processor) {
  }

  virtual ~OrderEventListener() {
  }

  virtual void OnComponentEvent(OrderEvent event, Order* order) {
    CHECK(processor_ != nullptr);
    processor_->OnOrderEvent(event, order);
  }

 private:
  OrderEventProcessor* processor_;
};

OrderEventProcessor::OrderEventProcessor()
    : order_component_(nullptr) {
}

OrderEventProcessor::~OrderEventProcessor() {
}

void OrderEventProcessor::OnInit() {
  CHECK(order_component_ != nullptr);
  order_component_->BeginWatch(new OrderEventListener(this));
}

void OrderEventProcessor::Process(octopus::Message* message) {
  // do nothing
}

void OrderEventProcessor::OnOrderEvent(OrderEvent event, Order* order) {
  switch (order->state()) {
    case Order::STATE_CREATED:
      OnCreated(order);
      break;
    case Order::STATE_CALCULATED:
      OnCalculated(order);
      break;
    case Order::STATE_RISKED:
      OnRisked(order);
      break;
    case Order::STATE_OPENED:
    case Order::STATE_DONE:
      OnDone(order);
      break;
    case Order::STATE_CANCELLED:
      break;
    default:
      break;
  }
}

void OrderEventProcessor::OnCreated(Order* order) {
  // TODO: create FareMessage and send out
  VLOG(99) << "OnCreated: " << order->order_id();
  FareResult result;
  order->OnCalcResult(&result);
}

void OrderEventProcessor::OnCalculated(Order* order) {
  // TODO: create AssessMessage and send out
  VLOG(99) << "OnCalculated: " << order->order_id();
  RiskResult result;
  order->OnRiskResult(&result);
}

void OrderEventProcessor::OnRisked(Order* order) {
  // TODO: create ReportMessage and send out
  VLOG(99) << "OnRisked: " << order->order_id();
  ExecutionReport report;
  order->OnExecutionReport(&report);
}

void OrderEventProcessor::OnDone(Order* order) {
  VLOG(99) << "OnDone(executed): " << order->order_id();
  completed_.push_back(order->entity_id());
}

void OrderEventProcessor::OnIdle(long count) {
  OrderAggregate* aggregate = order_component_->getAggregate();
  for (int i = 0; i < completed_.size(); ++i) {
    aggregate->RemoveModel(completed_[i]);
  }

  completed_.clear();
}

}  // namespace manatee
