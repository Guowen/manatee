/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstring>
#include <cstdio>
#include <glog/logging.h>

#include "order_aggregate.h"

#include "core/order.h"

namespace manatee {

OrderAggregate::OrderAggregate()
    : next_id_(1) {
}

OrderAggregate::~OrderAggregate() {
}

Order* OrderAggregate::CreateModel() {
  Order* order = new Order();
  long id = next_id_++;
  order->set_entity_id(id);

  char buffer[8] = {0};
  snprintf(buffer, 8, "%ld", id);
  order->set_order_id(std::string(buffer));

  VLOG(98) << "create order: " << id;

  return order;
}

bool OrderAggregate::FillModel(Order* order) {
  return false;
}

void OrderAggregate::PutModel(Order* order) {
  auto it = orders_.find(order->entity_id());
  if (it == orders_.end()) {
    orders_.insert(std::make_pair(order->entity_id(), order));
  }
}

bool OrderAggregate::RemoveModel(Order* order) {
  return RemoveModel(order->entity_id());
}

bool OrderAggregate::RemoveModel(long id) {
  auto it = orders_.find(id);
  if (it == orders_.end()) {
    return false;
  }

  VLOG(98) << "delete order: " << id;

  delete it->second;
  orders_.erase(it);
  return true;
}

Order* OrderAggregate::GetModel(long id) {
  auto it = orders_.find(id);
  return it == orders_.end() ? nullptr : it->second;
}

void OrderAggregate::RecycleModel(Order* order) {}

}  // namespace manatee
