/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_ORDER_ORDER_COMPONENT_H_
#define SRC_ORDER_ORDER_COMPONENT_H_

#include <memory>
#include <glog/logging.h>

#include "core/order.h"
#include "core/order_event.h"
#include "facilities/component.h"

namespace manatee {

struct NewOrderSingle;
struct OrderCancelRequest;
struct ExecutionReport;
struct OrderCancelReject;

class FareResult;
class RiskResult;

class OrderAggregate;

class OrderComponent : public Component<OrderEvent, Order*> {
 public:
  OrderComponent();
  virtual ~OrderComponent();

  OrderAggregate* getAggregate() {
    CHECK(aggregate_ != nullptr);
    return aggregate_.get();
  }

  void OnNewOrderSingle(NewOrderSingle* request, Order* order);
  void OnOrderCancelRequest(OrderCancelRequest* request);
  void OnExecutionReport(ExecutionReport* report);
  void OnOrderCancelReject(OrderCancelReject* reject);

  void OnCalcResult(FareResult* result);
  void OnRiskResult(RiskResult* result);

 private:
  void OnStateChanged(Order* order, int state);
  long ParseOrderID(const std::string& cl_ord_id);

  StateChangedCallback cb_;
  std::unique_ptr<OrderAggregate> aggregate_;
};

}

#endif /* SRC_ORDER_ORDER_COMPONENT_H_ */
