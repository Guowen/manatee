/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "order_processor.h"

#include "core/order.h"
#include "core/new_order_single.h"
#include "core/execution_report.h"

#include "order_component.h"
#include "order_aggregate.h"

namespace manatee {

OrderProcessor::OrderProcessor()
    : order_component_(nullptr) {
}

OrderProcessor::~OrderProcessor() {
}

void OrderProcessor::set_order_component(
    const std::shared_ptr<OrderComponent>& order_component) {
  CHECK(order_component != nullptr);
  order_component_ = order_component;
}

OrderAggregate* OrderProcessor::aggregate() {
  CHECK(order_component_ != nullptr);
  return order_component_->getAggregate();
}

void OrderProcessor::OnNewOrderSingle(NewOrderSingle* request) {
  // LOG(INFO) << "OnNewOrderSingle: " << request->account_id;

  CHECK(request != nullptr);
  CHECK(order_component_ != nullptr);
  // FIXME: validate the request and locate the account
  Order* order = aggregate()->CreateModel();
  // FIXME: setup order according to account info.
  order_component_->OnNewOrderSingle(request, order);
  aggregate()->PutModel(order);
}

void OrderProcessor::OnExecutionReport(ExecutionReport* report) {
  CHECK(report != nullptr);
  CHECK(order_component_ != nullptr);

  long id = 0;
  Order* order = aggregate()->GetModel(id);
  order_component_->OnExecutionReport(report);
}

void OrderProcessor::Process(octopus::Message* message) {
  CHECK(message != nullptr);
  if (message->type() != "OBJECT") {
    LOG(WARNING)<<"message type is not as expected: " << message->type();
    return;
  }

  CHECK(message->attachment() != nullptr);

  const std::string& topic = message->topic();
  if (topic == "NewOrderSingle") {
    OnNewOrderSingle((NewOrderSingle*) message->attachment());
  } else if (topic == "ExecutionReport") {
    OnExecutionReport((ExecutionReport*) message->attachment());
  } else {
    LOG(WARNING) << "message topic isn't as expected: " << message->topic();
  }
}

}
  // namespace manatee
