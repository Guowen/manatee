/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_BENCHMARK_PROCESSOR_H_
#define SRC_BENCHMARK_PROCESSOR_H_

#include <glog/logging.h>
#include "octopus/ring_producer_processor.h"

namespace manatee {

class BenchmarkProcessor : public octopus::RingProducerProcessor {
 public:
  BenchmarkProcessor();
  virtual ~BenchmarkProcessor();

  virtual void Process(octopus::Message* message) {
  }

  virtual void OnTimer(long milli_seconds);

  long ElapseToStart() {
    return Now() - begin_ts_;
  }

  static long Now() {
    timeval tv;
    memset(&tv, 0, sizeof(tv));
    gettimeofday(&tv, NULL);
    return tv.tv_sec;
  }

 private:
  void CloseOrders();
  long GetNextId() {
    return next_id_++;
  }

  std::string FormatId(long id);

  long count_;
  int interval_;
  int num_requests_per_interval_;
  long begin_ts_;
  long last_print_ts_;

  long next_id_;
  long opened_order_id_;
  long closed_order_id_;
};

}  // namespace manatee

#endif /* SRC_BENCHMARK_PROCESSOR_H_ */
