/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "benchmark_processor.h"

#include <gflags/gflags.h>
#include "core/new_order_single.h"
#include "core/execution_report.h"
#include "order/order_component.h"
#include "order/order_processor.h"
#include "order/order_event_processor.h"

DEFINE_int32(num_requests, 10000, "num requests per seconds");

namespace manatee {

BenchmarkProcessor::BenchmarkProcessor()
    : count_(0),
      interval_(1),
      num_requests_per_interval_(10),
      begin_ts_(Now()),
      last_print_ts_(0),
      next_id_(1),
      opened_order_id_(0),
      closed_order_id_(0) {
  if (FLAGS_num_requests > 1000) {
    interval_ = 1;
    num_requests_per_interval_ = FLAGS_num_requests / 1000;
  } else {
    interval_ = 1000 / FLAGS_num_requests;
    num_requests_per_interval_ = 1;
  }

  LOG(INFO) << "interval: " << interval_ << ", requests: "
      << num_requests_per_interval_;
}

BenchmarkProcessor::~BenchmarkProcessor() {
}

void BenchmarkProcessor::OnTimer(long milli_seconds) {
  if (milli_seconds % interval_ == 0) {
    for (int i = 0; i < num_requests_per_interval_; ++i) {
      octopus::Message* message = Provide();
      if (message == nullptr) {
        LOG(INFO)<< "provide message failed";
        return;
      }

      message->set_subject("order");
      message->set_type("OBJECT");
      message->set_topic("NewOrderSingle");

      NewOrderSingle* new_order_single = new NewOrderSingle();
      new_order_single->account_id = "000001";
      new_order_single->ord_type = OrdType::MARKET;
      new_order_single->order_qty = 100.0;
      new_order_single->price = 10.00;
      new_order_single->security_id = "000776";
      new_order_single->security_id_source = "102";

      message->reset_attachment(reinterpret_cast<void*>(new_order_single));

      ++count_;
      Publish();
    }

    long now = ElapseToStart();
    if (now != last_print_ts_) {
      std::cout << "time: " << now << " count: " << count_ << std::endl;
      last_print_ts_ = now;
    }
  }
}

void BenchmarkProcessor::CloseOrders() {
  for (long id = closed_order_id_ + 1; id <= opened_order_id_; ++id) {
    octopus::Message* message = Provide();
    if (message == nullptr) {
      LOG(INFO) << "provide message failed";
      return;
    }

    std::string id_str(FormatId(id));

    message->set_subject("order");
    message->set_type("OBJECT");
    message->set_topic("ExecutionReport");
    ExecutionReport* report = new ExecutionReport();
    report->order_id = id_str;
    report->cl_ord_id = id_str;

    message->reset_attachment(report);
    closed_order_id_ = id;

    Publish();
  }
}

std::string BenchmarkProcessor::FormatId(long id) {
  char buffer[8] = { 0 };
  snprintf(buffer, 8, "%ld", id);
  return std::string(buffer);
}

}  // namespace manatee
