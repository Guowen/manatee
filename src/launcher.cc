/*
 * Copyright 2016 Guowen Shen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "launcher.h"

#include <gflags/gflags.h>
#include "octopus/ring_producer_processor.h"
#include "octopus/pipeline.h"
#include "octopus/thread_running_environment.h"
#include "octopus/bus/front_sender.h"
#include "octopus/bus/front_send_processor.h"
#include "octopus/bus/front_receiver.h"
#include "octopus/bus/front_recv_processor.h"

#include "order/order_component.h"
#include "order/order_processor.h"
#include "order/order_event_processor.h"
#include "benchmark_processor.h"

namespace manatee {

class InSingleProcessLauncher : public Launcher {
 public:
  InSingleProcessLauncher() {
  }
  virtual ~InSingleProcessLauncher() {
  }

  virtual void Init() {
    // init order handling processor
    std::shared_ptr<OrderComponent> order_component(new OrderComponent());
    order_component_ = order_component;
    order_processor_.reset(new OrderProcessor());
    order_event_processor_.reset(new OrderEventProcessor());
    order_processor_->set_order_component(order_component);
    order_event_processor_->set_order_component(order_component);

    // init create orders processor
    benchmark_.reset(new BenchmarkProcessor());

    // init pipeline
    pipeline_.reset(new octopus::Pipeline());
    pipeline_->SetProducer(benchmark_->CreateRingProducer());       // 生成订单
    pipeline_->AddConsumer(order_processor_->CreateRingConsumer()); // 处理订单
    pipeline_->Init();

    // creating orders in a thread
    running_env_.reset(new octopus::ThreadRunningEnvironment());
    running_env_->AddProcessor(benchmark_.get());

    // handling orders in another thread
    running_env2_.reset(new octopus::ThreadRunningEnvironment());
    running_env2_->AddProcessor(order_processor_.get());
    running_env2_->AddProcessor(order_event_processor_.get());

    running_env_->Init();
    running_env2_->Init();
  }

  virtual void Launch() {
    running_env_->Loop(true);
    running_env2_->Run();
  }

 private:
  std::unique_ptr<octopus::Pipeline> pipeline_;

  std::unique_ptr<BenchmarkProcessor> benchmark_;
  std::shared_ptr<OrderComponent> order_component_;
  std::unique_ptr<OrderProcessor> order_processor_;
  std::unique_ptr<OrderEventProcessor> order_event_processor_;

  std::unique_ptr<octopus::ThreadRunningEnvironment> running_env_;
  std::unique_ptr<octopus::ThreadRunningEnvironment> running_env2_;
};

class OrderSendingPipeline {
 public:
  void Init() {
    // init send endpoint
    octopus::Endpoint endpoint("send");
    endpoint.set_address("inproc://benchmark");

    sender_.reset(new octopus::FrontSender());
    sender_->AddEndpoint(endpoint);

    send_.reset(new octopus::FrontSendProcessor());
    send_->set_front_sender(sender_.get());

    benchmark_.reset(new BenchmarkProcessor());

    pipeline_.reset(new octopus::Pipeline());
    pipeline_->SetProducer(benchmark_->CreateRingProducer());   // 生成订单
    pipeline_->AddConsumer(send_->CreateRingConsumer());        // 网络发送
    pipeline_->Init();

    // create orders and send orders in a thread
    running_env_.reset(new octopus::ThreadRunningEnvironment());
    running_env_->AddProcessor(benchmark_.get());
    running_env_->AddProcessor(send_.get());
  }

  void Run() {
    running_env_->Loop(true);
  }

 private:
  std::unique_ptr<octopus::FrontSender> sender_;
  std::unique_ptr<octopus::Pipeline> pipeline_;
  std::unique_ptr<BenchmarkProcessor> benchmark_;
  std::unique_ptr<octopus::FrontSendProcessor> send_;
  std::unique_ptr<octopus::ThreadRunningEnvironment> running_env_;
};

class OrderProcessingPipeline {
 public:
  OrderProcessingPipeline() {
  }
  ~OrderProcessingPipeline() {
  }

  void Init() {
    // init receive endpoint
    octopus::Endpoint endpoint("recv");
    endpoint.set_address("inproc://benchmark");

    receiver_.reset(new octopus::FrontReceiver());
    receiver_->AddEndpoint(endpoint);

    recv_.reset(new octopus::FrontRecvProcessor());
    recv_->set_front_receiver(receiver_.get());

    std::shared_ptr<OrderComponent> order_component(new OrderComponent());
    order_component_ = order_component;
    order_processor_.reset(new OrderProcessor());
    order_event_processor_.reset(new OrderEventProcessor());
    order_processor_->set_order_component(order_component);
    order_event_processor_->set_order_component(order_component);

    pipeline_.reset(new octopus::Pipeline());
    pipeline_->SetProducer(recv_->CreateRingProducer());            // 网络接收
    pipeline_->AddConsumer(order_processor_->CreateRingConsumer()); // 处理订单
    pipeline_->Init();

    // receive orders in a thread
    running_env_.reset(new octopus::ThreadRunningEnvironment());
    running_env_->AddProcessor(recv_.get());

    // handling orders in another thread
    running_env2_.reset(new octopus::ThreadRunningEnvironment());
    running_env2_->AddProcessor(order_processor_.get());
    running_env2_->AddProcessor(order_event_processor_.get());
  }

  void Run() {
    running_env_->Loop(true);
  }

 private:
  std::shared_ptr<OrderComponent> order_component_;

  std::unique_ptr<octopus::FrontReceiver> receiver_;
  std::unique_ptr<octopus::Pipeline> pipeline_;
  std::unique_ptr<octopus::FrontRecvProcessor> recv_;
  std::unique_ptr<OrderProcessor> order_processor_;
  std::unique_ptr<OrderEventProcessor> order_event_processor_;

  std::unique_ptr<octopus::ThreadRunningEnvironment> running_env_;
  std::unique_ptr<octopus::ThreadRunningEnvironment> running_env2_;
};

class NetworkLauncher : public Launcher {
 public:
  NetworkLauncher() {
  }
  virtual ~NetworkLauncher() {
  }

  virtual void Init() {
    order_sending_.reset(new OrderSendingPipeline());
    order_sending_->Init();

    order_processing_.reset(new OrderProcessingPipeline());
    order_processing_->Init();
  }

  virtual void Run() {
    order_sending_->Run();
    order_processing_->Run();

    while (true)
      sleep(1);
  }

 private:
  std::unique_ptr<OrderSendingPipeline> order_sending_;
  std::unique_ptr<OrderProcessingPipeline> order_processing_;
};

Launcher* Launcher::CreateLauncher(const Launcher::Mode mode) {
  if (mode == Launcher::IN_SINGLE_PROCESS) {
    return new InSingleProcessLauncher();
  }

  return nullptr;
}

}
